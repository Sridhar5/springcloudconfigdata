package com.sample.client.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MessageController {
	@Value("${msg}") 
	String message;
	
	@GetMapping("/msg")
	public String showMsg() {
		return "The msg is "+message;
	}
}
